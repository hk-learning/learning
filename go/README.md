go-zero
https://go-zero.dev/cn/

go-im
https://github.com/fanux/lhttp

由浅入深剖析 go channel
https://www.jianshu.com/p/24ede9e90490

CSP与并发编程
https://www.sohu.com/a/192606128_575744

十分钟读懂：Java并发——CSP模型
https://www.jianshu.com/p/efafa51d3066

Go 资源大全中文版
https://github.com/jobbole/awesome-go-cn

Go语言高级编程(Advanced Go Programming)
https://chai2010.cn/advanced-go-programming-book/

Golang 中文学习资料
https://go.wuhaolin.cn/gopl/ch9/ch9-07.html

浅谈 Go 语言实现原理
https://draveness.me/golang/concurrency/golang-channel.html

Micro文档
https://micro.mu/docs/cn/go-micro-internals.html

xorm
https://www.kancloud.cn/xormplus/xorm/167078

分布式系统中的一致性与共识算法
http://www.xuyasong.com/?p=1970

Go的变量到底在堆还是栈中分配
https://studygolang.com/articles/7559

TCP 三次握手四次挥手
https://www.cnblogs.com/onesea/p/13053697.html

Golang服务平滑重启， grace,endless,overseer
https://blog.csdn.net/u013474436/article/details/104761835/
http://www.ay1.cc/article/1673234109851923189.html

基于Supervisor热重启golang
https://www.jianshu.com/p/f1b16543af6d

epoll
https://zhuanlan.zhihu.com/p/56486633

Golang基础 GMP
https://zhuanlan.zhihu.com/p/466593330

Golang语言介绍
http://c.biancheng.net/golang/intro/

性能分析
http://c.biancheng.net/view/125.html

面试题
https://www.zhihu.com/tardis/bd/art/519979757?source_id=1001

golang 面试题(从基础到高级)
https://blog.csdn.net/Bel_Ami_n/article/details/123352478
