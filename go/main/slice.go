package main

import "fmt"

/**
https://www.jianshu.com/p/62a2f6f08845
*/
func main() {
	// a. 使用内置的make函数
	//只指定长度，则默认容量和长度相等
	//容量的定义在后面解释
	slice1 := make([]string, 5)
	//指定长度和容量，容量不能小于长度
	slice2 := make([]string, 3, 5)

	//b. 使用切片字面量
	slice3 := []string{"dog", "cat", "bear"}
	slice4 := []int{99: 0}

	fmt.Printf("%#v\n", slice1)
	fmt.Printf("%#v\n", slice2)
	fmt.Printf("%#v\n", slice3)
	fmt.Printf("%#v\n", slice4)
}
