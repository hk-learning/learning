module main

go 1.14

require (
	gorm.io/driver/mysql v1.5.1
	gorm.io/gorm v1.25.1
)
