
func (*roomBusi) OptionSave(ctx context.Context, req *bcpb.ReqOptionSave, reply *bcpb.RepOptionSave) error {
	uri := "http://192.168.11.60:6080/option/save" 
	var bd strings.Builder
	bd.WriteString(uri)
	bd.WriteString("?package=sg.partying.android.lite")
	bd.WriteString("&uid=")
	bd.WriteString(gconv.String(req.Uid))
	bd.WriteString("&rid=")
	bd.WriteString(gconv.String(req.Rid))
	bd.WriteString("&version=")
	bd.WriteString(gconv.String(req.Version))
	bd.WriteString("&puzzle_id=")
	bd.WriteString(gconv.String(req.PuzzleId))
	bd.WriteString("&unity_version=")
	bd.WriteString(req.UnityVersion)
	bd.WriteString("&options=")
	bd.WriteString(req.Options)
	url := bd.String()

	res, err := http.Get(url)
	if err != nil {
		g.Log().Errorf("http.Get failed req=%s err=%v", req.String(), err)
		return err
	}
	defer res.Body.Close()

	bys, err := ioutil.ReadAll(res.Body)
	if err != nil {
		g.Log().Errorf("ioutil.ReadAll failed req=%s err=%v", req.String(), err)
		return err
	}

	if err = json.Unmarshal(bys, reply); err != nil {
		g.Log().Errorf("json.Unmarshal failed bys=%s req=%s err=%v", string(bys), req.String(), err)
		return err
	}

	g.Log().Debug("==========OptionSave over", url, req.String(), reply.String())
	return nil
}
