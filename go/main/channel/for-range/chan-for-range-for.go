package main

import "fmt"

func main() {
	ch := make(chan int, 10)
	ch <- 1
	ch <- 2

	close(ch)

	for {
		v, ok := <-ch
		if !ok {
			break
		}
		fmt.Printf("for ch v: %d\n", v)
	}

	x, ok := <-ch
	fmt.Printf("x:%d, ok:%v := <-ch\n", x, ok)
}
