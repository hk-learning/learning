package main

import (
	"fmt"
	"time"
)

func main() {
	var ch chan int
	ch = make(chan int, 0)
	ch2 := make(chan int, 0)

	go func() {
		for v := range ch {
			fmt.Printf("for range ch v: %d\n", v)
		}
	}()

	go func() {
		for v := range ch2 {
			fmt.Printf("for range ch2 v: %d\n", v)
		}
	}()

	ch <- 1
	ch <- 2
	ch <- 3
	ch2 <- 1
	ch2 <- 2
	ch2 <- 3
	time.Sleep(time.Second * 5) //睡眠5
	ch <- 4
	ch <- 5
	ch2 <- 4
	ch2 <- 5
	close(ch)
	close(ch2)
}
