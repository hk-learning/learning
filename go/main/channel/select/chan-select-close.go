package main

import "fmt"

//https://blog.csdn.net/qq_37102984/article/details/128152775
//Go channel的广播机制：
//当一个channel被 close时，所有通过select监听这个channel IO事件的goroutine，都会收到相关事件：当前监听的channel已关闭。

//go中channel和goroutine以及select的配合使用
//https://blog.csdn.net/qq_39162386/article/details/89362685

func main() {
	ch := make(chan int, 2)
	for i := 1; i <= 2; i++ {
		ch <- i
	}

	close(ch) // 关闭channel，v1、v2、v3都会收到相关事件

	// Go channel的广播机制：三个select都监听到了相关事件
	select {
	case v1 := <-ch:
		fmt.Println("v1: ", v1) // 输出：1
	}

	select {
	case v2 := <-ch:
		fmt.Println("v2: ", v2) // 输出：2
	}

	select {
	case v3 := <-ch:
		fmt.Println("v3: ", v3) // 输出：0，channel读完为空时，后续读值都为0值
	}
}
