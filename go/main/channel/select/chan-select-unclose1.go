package main

import "fmt"

//https://blog.csdn.net/qq_37102984/article/details/128152775
//遍历未关闭的channel时会导致死锁阻塞：
//如果不close掉channel直接range遍历是会发生死锁的，
//原因是 当for循环读完channel的10个值之后会继续尝试读取下一个，而由于channel为空又没关闭，会一直阻塞形成死锁。

func main() {
	var ch chan int
	var limit = 10
	var cnt int
	ch = make(chan int, limit)

	for i := 0; i < limit; i++ {
		ch <- i
	}

	for {
		if cnt == limit { //todo 虽然channel没有close，但是此处避免去读取空的ch
			fmt.Printf("for cnt:%d, limit:%d\n", cnt, limit)
			break
		}

		select {
		case v := <-ch:
			cnt++
			fmt.Printf("select get ch: %d\n", v)
		}
	}

}
