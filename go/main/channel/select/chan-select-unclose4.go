package main

import (
	"fmt"
	"time"
)

//注意：
//1.select可以同时监听多个channel的写入或读取
//2.执行select若只有一个case不阻塞，则执行这个case块
//3.若多个case不阻塞，则随机挑选一个执行
//4.若所有case都阻塞则实行default块，若未定义default则select语句阻塞，直到有case被唤醒
//5.使用break跳出select

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	ch3 := make(chan int)
	ch4 := make(chan int)

	go func() {
		fmt.Println("1")
		ch1 <- 1
		fmt.Println("2")
		ch2 <- 2
		fmt.Println("3")
		ch3 <- 3
	}()

	time.Sleep(time.Second * 1)

	select {
	case <-ch1:
		fmt.Println("A")
	case <-ch2:
		fmt.Println("B")
	case <-ch3:
		fmt.Println("C")
	case <-ch4:
		fmt.Println("D")
	default:
		//fmt.Println("E") //全部阻塞，执行default
	}

	fmt.Println("F")
}

//输出：
//1
//2
//A
//F

// or

//1
//A
//F
