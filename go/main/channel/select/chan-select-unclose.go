package main

import (
	"fmt"
	"time"
)

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	ch3 := make(chan int)
	ch4 := make(chan int)
	var cnt int
	go func() {
		fmt.Println("1")
		ch1 <- 1
		fmt.Println("2")
		ch2 <- 2
		fmt.Println("3")
		ch3 <- 3
		fmt.Println("4")
		ch4 <- 4
	}()

	time.Sleep(time.Second * 1)

	for {
		if cnt == 4 {
			break
		}

		select {
		case <-ch1:
			fmt.Println("A")
			cnt++
		case <-ch2:
			fmt.Println("B")
			cnt++
		case <-ch3:
			fmt.Println("C")
			cnt++
		case <-ch4:
			fmt.Println("D")
			cnt++
		default:
			//fmt.Println("E") //全部阻塞，执行default
		}
	}

	fmt.Println("F")
}

//输出：
//1
//A
//2
//B
//3
//C
//4
//D
//F
