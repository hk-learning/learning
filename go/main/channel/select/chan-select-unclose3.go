package main

import (
	"fmt"
)

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	ch3 := make(chan int)
	ch4 := make(chan int)

	select {
	case <-ch1:
		fmt.Println("A")
	case <-ch2:
		fmt.Println("B")
	case <-ch3:
		fmt.Println("C")
	case <-ch4:
		fmt.Println("D")
	default:
		fmt.Println("E") //全部阻塞，执行default
	}

	fmt.Println("F")
}

//输出：
//E
//F
