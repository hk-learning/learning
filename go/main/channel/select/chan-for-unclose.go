package main

import "fmt"

//https://blog.csdn.net/qq_37102984/article/details/128152775
//遍历未关闭的channel时会导致死锁阻塞：
//如果不close掉channel直接range遍历是会发生死锁的，
//原因是 当for循环读完channel的10个值之后会继续尝试读取下一个，而由于channel为空又没关闭，会一直阻塞形成死锁。

func main() {
	var ch chan int
	var limit = 10
	ch = make(chan int, limit)

	for i := 0; i < limit; i++ {
		ch <- i
	}

	// 遍历时，如果channel没有关闭，则会出现deadlock错误
	//todo ch没有close，读取空ch会报错 `fatal error: all goroutines are asleep - deadlock!`

	//close(ch) // 注释掉，不关闭

	for {
		v, ok := <-ch
		if !ok {
			break
		}
		fmt.Printf("for ch v: %d\n", v)
	}
}
