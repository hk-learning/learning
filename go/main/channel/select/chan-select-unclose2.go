package main

import "fmt"

//https://blog.csdn.net/qq_37102984/article/details/128152775
//遍历未关闭的channel时会导致死锁阻塞：
//如果不close掉channel直接range遍历是会发生死锁的，
//原因：当for循环读完channel的10个值之后会继续尝试读取下一个，而由于channel为空又没关闭，会一直阻塞形成死锁。
func main() {
	ch := make(chan int, 10)
	for i := 1; i <= 10; i++ {
		ch <- i
	}

	// todo 遍历时，如果channel没有关闭，则会出现deadlock错误
	//close(ch) // 注释掉，不关闭

	// channel遍历
	fmt.Println("start...")
	for v := range ch {
		fmt.Println("v: ", v) // 输出1、2后就会阻塞
	}

	fmt.Println("end...")
}

/*
仅输出:
start...
v:  1
v:  2
*/
