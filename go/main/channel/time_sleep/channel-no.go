package main

import (
	"log"
	"time"
)

//测试结束, elapsed:23.01097525s
func main() {
	t := time.Now()
	log.Println("开始测试...")
	var limit = 10
	for i := 0; i < limit; i++ {
		time.Sleep(time.Second * 2)
		log.Println(i, "done")
	}
	log.Println("循环结束")
	time.Sleep(time.Second * 3) //等待执行完成
	elapsed := time.Since(t)
	log.Printf("测试结束, elapsed:%+v", elapsed)
}
