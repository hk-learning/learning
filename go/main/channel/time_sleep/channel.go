package main

import (
	"log"
	"time"
)

//测试结束, elapsed:11.005703s
func main() {
	t := time.Now()
	log.Println("开始测试...")
	var limit = 10
	var cnt int
	//var ticker = time.NewTicker(time.Second * 3) //3s超时
	ch := make(chan int, 2) //(最大允许并发数)设置为2
	for i := 0; i < limit; i++ {
		//尝试增加一个协程, 若已达到最大并发数,将阻塞
		ch <- 1
		go func(i int) {
			time.Sleep(time.Second * 2)
			log.Println(i, "done")
			<-ch
			cnt++
		}(i)
		//go func(ch chan int, i int) {
		//	time.Sleep(time.Second * 2)
		//	log.Println(i, "done")
		//}(ch, i)
		//go func(i int) {
		//	time.Sleep(time.Second * 2)
		//	log.Println(i, "done")
		//}(i)
	}
	//
	//for {
	//	if cnt == limit {
	//		break
	//	}
	//
	//	select {
	//	case <-ticker.C:
	//		log.Printf("timeout limit:%d, cnt:%d", limit, cnt)
	//	case <-ch: //一个并发协程已经完成
	//		log.Printf("<-ch limit:%d, cnt:%d", limit, cnt)
	//	}
	//}

	log.Println("循环结束")
	time.Sleep(time.Second * 3) //等待执行完成
	elapsed := time.Since(t)
	log.Printf("测试结束, elapsed:%+v", elapsed)
}
