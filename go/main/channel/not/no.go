package main

import (
	"fmt"
	"net"
	"sync"
	"time"
)

const host = "127.0.0.1"

type Job struct {
	host string
	port int
}

type Result struct {
	job    Job
	status bool
}

var jobs = make(chan Job)
var results = make(chan Result)

func main() {
	wg := sync.WaitGroup{}

	go func() {
		for i := 1; i < 65535; i++ {
			jobs <- Job{host, i}
		}
		close(jobs)
	}()

	go func() {
		for result := range results {
			if result.status {
				fmt.Println(result.job, "open")
			}
		}
	}()

	limit := 4500
	wg.Add(limit)
	for i := 0; i < limit; i++ {
		//wg.Add(1)
		go worker(&wg)
	}

	wg.Wait()
}

func worker(wg *sync.WaitGroup) {
	for job := range jobs {
		_, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", job.host, job.port), time.Millisecond*1500)
		if err != nil {
			results <- Result{job, false}
		} else {
			results <- Result{job, true}
		}
	}
	wg.Done()
}
