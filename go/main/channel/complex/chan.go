package main

import (
	"fmt"
	"time"
)

// 从无缓存的 channel 中读取消息会阻塞，直到有 goroutine 向该 channel 中发送消息；
// 同理，向无缓存的 channel 中发送消息也会阻塞，直到有 goroutine 从 channel 中读取消息。
//
// 有缓存的 channel 类似一个阻塞队列(采用环形数组实现)。
// 当缓存未满时，向 channel 中发送消息时不会阻塞，
// 当缓存满时，发送操作将被阻塞，直到有其他 goroutine 从中读取消息；
// 相应的，
// 当 channel 中消息不为空时，读取消息不会出现阻塞，
// 当 channel 为空时，读取操作会造成阻塞，直到有 goroutine 向 channel 中写入消息。
func main() {
	singleCoroutine()
	multiCoroutine()
}

// 单协程，只能用到CPU的一个核
func singleCoroutine() {
	var result, i uint64
	start := time.Now()
	for i = 1; i <= 10000000000; i++ {
		result += i
	}
	elapsed := time.Since(start)
	fmt.Println(elapsed, result) // 4.330357206s 13106511857580896768
}

// 多协程
func multiCoroutine() {
	var result uint64
	start := time.Now()
	ch1 := calc(1, 2500000000)
	ch2 := calc(2500000001, 5000000000)
	ch3 := calc(5000000001, 7500000000)
	ch4 := calc(7500000001, 10000000000)
	// 主协程需要与子协程通信，Go中协程之间的通信推荐使用channel
	result = <-ch1 + <-ch2 + <-ch3 + <-ch4
	// ch1只能读取数据，如果通过ch1写入数据，编译时会报错
	// ch1 <- 7 // invalid operation: ch1 <- 7 (send to receive-only type <-chan uint64)
	elapsed := time.Since(start)
	fmt.Println(elapsed, result) // 1.830920702s 13106511857580896768
}

// 返回一个只能接收数据的channel
// 方法创建的子协程会把计算结果发送到这个channel，而主协程会通过channel把计算结果取出来
func calc(from uint64, to uint64) <-chan uint64 {
	// channel用于协程间的通信，这是一个无缓冲的channel
	channel := make(chan uint64)
	go func() {
		result := from
		for i := from + 1; i <= to; i++ {
			result += i
		}
		// 将结果写入channel
		channel <- result
	}()
	// 返回用于通信的channel
	return channel
}
