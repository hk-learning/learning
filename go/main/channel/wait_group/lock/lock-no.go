package main

import (
	"fmt"
	"sync"
)

var num2 int
var wg2 sync.WaitGroup

func sub2() {
	defer wg2.Done()
	for i := 0; i < 100000; i++ {
		num2 -= 1
	}
}

func add2() {
	defer wg2.Done()
	for i := 0; i < 100000; i++ {
		num2 += 1
	}
}

func main() {
	wg2.Add(2)
	// 启动两个协程，分别是加1和减1两个函数
	go add2()
	go sub2()
	// 此时等待两个协程执行完毕
	wg2.Wait()
	// 接着执行主协程的逻辑
	fmt.Println(num2)
}
