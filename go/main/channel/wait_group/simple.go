package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	var limit = 10
	wg.Add(limit)
	for i := 0; i < limit; i++ {
		go func(i int) {
			fmt.Println("go func i:", i)
			wg.Add(-1)
		}(i)
	}

	wg.Wait()
}
