package main

import (
	"fmt"
	"sync"
	"time"
)

var limitNum = 10
var taskNum = 100
var jobGroup sync.WaitGroup

func main() {
	var i int
	var j int

	//组装任务
	chanTask := make(chan int, taskNum)
	for j = 0; j < taskNum; j++ {
		chanTask <- j
	}
	close(chanTask)

	jobGroup.Add(taskNum)
	for i = 0; i < limitNum; i++ { //最多10个协程
		go doTask(chanTask)
	}

	jobGroup.Wait()
	fmt.Println("main over")
}

func doTask(taskChan chan int) {
	for taskId := range taskChan { //每个协程拼命抢夺任务，直到任务完结
		time.Sleep(time.Millisecond * 100)
		fmt.Println("finish task ", taskId)
		jobGroup.Done()
	}
}
