package main

import (
	"fmt"
	"sync"
)

var wg3 sync.WaitGroup

func main() {
	// 创建一个容量为4的channel
	channel := make(chan int, 4)
	// 创建4个协程，作为生产者
	for i := 0; i < 4; i++ {
		wg3.Add(1)
		go func() {
			channel <- 7
		}()
	}

	// 创建4个协程，作为消费者
	for i := 0; i < 4; i++ {
		go func() {
			o := <-channel
			fmt.Println("received : ", o) // TODO  不加wg就没有输出
			wg3.Done()
		}()
	}

	wg3.Wait()
}
