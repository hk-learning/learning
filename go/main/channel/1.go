package main

import (
	"fmt"
)

func main() {
	//不阻塞
	var ch = make(chan bool)
	go func() {
		fmt.Println("C语言中文网")
		ch <- true
	}()

	<-ch
}
