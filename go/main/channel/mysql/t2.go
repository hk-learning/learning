package main

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"time"
)

//elapsed:3.911226958s, findCnt:99957
func main() {
	t := time.Now()
	// 连接数据库
	dsn := "root:123456@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return
	}

	type songModel struct {
		Id    uint32
		Name  string
		Phone string
	}

	var cnt = 100000
	var queryCh = make(chan int, 10)
	var findCnt int
	for i := 0; i < cnt; i++ {
		queryCh <- 1
		go func() {
			var song songModel
			db.Table("xs_ktv_song").Where("id=?", 1).Find(&song)
			<-queryCh
			findCnt++
		}()
	}

	elapsed := time.Since(t)

	fmt.Printf("elapsed:%+v, findCnt:%+v \n", elapsed, findCnt)
}
