package main

import (
	"log"
	"time"
)

type GoLimit struct {
	ch chan int
}

func NewGoLimit(max int) *GoLimit {
	return &GoLimit{ch: make(chan int, max)}
}

func (g *GoLimit) Add() {
	g.ch <- 1
}

func (g *GoLimit) Done() {
	<-g.ch
}

func main() {
	log.Println("开始测试...")
	g := NewGoLimit(2) //max_num(最大允许并发数)设置为2
	for i := 0; i < 10; i++ {
		//尝试增加一个协程, 若已达到最大并发数,将阻塞
		g.Add()
		go func(g *GoLimit, i int) {
			defer g.Done() //一个并发协程已经完成
			time.Sleep(time.Second * 2)
			log.Println(i, "done")
		}(g, i)
	}
	log.Println("循环结束")
	time.Sleep(time.Second * 3) //等待执行完成
	log.Println("测试结束")
}
