package main

import (
	"fmt"
)

//无缓存的 channel https://www.jianshu.com/p/24ede9e90490
//从无缓存的 channel 中读取消息会阻塞，直到有 goroutine 向该 channel 中发送消息；
//同理，向无缓存的 channel 中发送消息也会阻塞，直到有 goroutine 从 channel 中读取消息。
func main() {
	var ch = make(chan bool)
	go func() {
		fmt.Println("从无缓存的 channel 中读取消息会阻塞，直到有 goroutine 向该 channel 中发送消息；不阻塞")
		ch <- true
	}()

	<-ch
}
