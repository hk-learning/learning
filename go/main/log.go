package main

import (
	"fmt"
	"runtime"
)

func main() {
	a := 1
	b := 2
	c := a + b
	fmt.Print(c)
}

// log func
func Log(err error) {
	fmt.Println(runtime.Caller(0))
	fmt.Println(runtime.Caller(1))
	fmt.Println(runtime.Caller(2))
	fmt.Printf("%#v\n", err)
}
