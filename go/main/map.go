package main

import (
	"fmt"
)

func main() {
	ages := map[string]int{
		"allen": 32,
		"paul":  34,
	}

	fmt.Printf("%#v\n", ages)

	for k, v := range ages {
		fmt.Println(k, v)
	}
	// go语言遍历map和按序输出 https://www.cnblogs.com/noKing/p/11661567.html
	// runtime.mapiterinit()
	// golang底层并没有保证这一点，或许(现在/以后)会有特殊情况出现顺序不固定的情况。担心开发者们误解这一点，
	// golang就特意去打乱了这个顺序，让开发者们知道golang底层不保证map每次遍历都是同一个顺序。
}
