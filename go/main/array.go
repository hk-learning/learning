package main

import "fmt"

func main() {
	var arr0 [5]int = [5]int{1, 2, 3}
	arr1 := [5]int{1, 2, 3, 4, 5}
	arr2 := [5]string{"dog", "cat", "bear"}

	fmt.Printf("%#v\n", arr0)
	fmt.Printf("%#v\n", arr1)
	fmt.Printf("%#v\n", arr2)
}
