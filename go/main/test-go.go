package main

import (
	"fmt"
	"os"
	"time"
)

func main() {
	go while("goroutine1")
	go while("goroutine2")
	go while("goroutine3")
}

func appendToFile(content string) error {
	fileName := "./output.txt"
	// 以只写的模式，打开文件
	f, err := os.OpenFile(fileName, os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	defer f.Close()
	// 查找文件末尾的偏移量
	n, _ := f.Seek(0, os.SEEK_END)
	// 从末尾的偏移量开始写入内容
	_, err = f.WriteAt([]byte(content), n)
	return err
}

func while(name string) {
	for i := 0; i <= 100; i++ {
		time.Sleep(time.Duration(1) * time.Second)
		content := fmt.Sprintf("%s %d\n", name, i)
		fmt.Printf("%#v\n", content)
		err := appendToFile(content)
		if err != nil {

		}
	}
}
