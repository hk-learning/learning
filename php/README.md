EwoMail是基于Linux的企业邮箱服务器(https://github.com/gyxuehu/EwoMail)

PHP四层体系：
https://blog.csdn.net/qq_22933315/article/details/108830517

php-fpm的两种进程管理模式
https://blog.csdn.net/dreamboycx/article/details/50328181

PHP 底层的运行机制与数据结构原理
https://www.cnblogs.com/frankltf/p/9064419.html

一个完整的HTTP请求的详细过程
https://blog.csdn.net/Kanmeijie/article/details/110877946

WebSocket 网络协议
https://zhuanlan.zhihu.com/p/556813075

PHP-FPM
https://zhuanlan.zhihu.com/p/371292270

PHP-FPM对进程的管理
https://www.cnblogs.com/sanshuiqing/articles/14632748.html

PHP-FPM实现平滑重启
https://zhuanlan.zhihu.com/p/274571410

PHP平滑关闭/重启的实现方法
https://www.inte.net/news/247072.html
