<?php

$arr1 = [];
$arr2 = [];
$num = 200000;
for ($i = 1; $i <= $num; $i++) {
    $arr1[] = $i;
}
for ($i = $num - 10; $i <= $num + 10; $i++) {
    $arr2[] = $i;
}

$stime = microtime(1);
$arr3 = array_intersect($arr1, $arr2);
print_r($arr3);
echo 'array_intersect runtime:', microtime(1) - $stime, PHP_EOL;

$arr3 = [];
$stime = microtime(1);
foreach ($arr1 as $a1) {
    foreach ($arr2 as $a2) {
        if ($a1 == $a2) {
            $arr3[] = $a1;
        }
    }
}
print_r($arr3);
echo 'array double foreach runtime:', microtime(1) - $stime, PHP_EOL;

$arr3 = [];
$arr4 = [];
$stime = microtime(1);
foreach ($arr2 as $a2) {
    $arr4[$a2] = $a2;
}
foreach ($arr1 as $a1) {
    if (isset($arr4[$a1])) {
        $arr3[] = $a1;
    }
}
print_r($arr3);
echo 'array one foreach runtime:', microtime(1) - $stime, PHP_EOL;
