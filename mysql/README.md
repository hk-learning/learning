MySQL事务隔离级别和实现原理
https://zhuanlan.zhihu.com/p/117476959/

MySQL数据库—事务隔离级别和锁关系学习
https://www.cnblogs.com/fjg2572120/p/13479510.html

MySQL是怎么解决幻读问题的？
https://zhuanlan.zhihu.com/p/145822414

MySQL查看是否锁表并解除锁
https://www.jianshu.com/p/750904730726

MySQL查询是否锁表
https://www.cnblogs.com/chxl800/p/10498511.html

查询MySQL中是否表被锁
https://blog.csdn.net/tyyh08/article/details/81354023

Innodb锁机制：Next-Key Lock 浅谈
https://www.cnblogs.com/zhoujinyi/p/3435982.html

MySQL事务隔离级别
https://blog.csdn.net/Little_fxc/article/details/124196928

MySQL锁
https://blog.csdn.net/wgzblog/article/details/127281354

MySQL索引
https://www.cnblogs.com/zhangyi555/p/15596768.html

MySQL聚簇索引
https://mp.weixin.qq.com/s?__biz=MzAxMjY5NDU2Ng==&mid=2651861539&idx=1&sn=77e036f27b300dfddc979842f3255b30&chksm=8049776ab73efe7cdc3c777e6c499bfcd92538d97210e8b745e7402d2e1068367a9877f8dca4&scene=27

```
共享锁：指该锁可以被多个线程锁持有
独占锁：指该锁一次只能被一个线程所持有
MySQL高性能中提到：串行化会在读取的每一行数据上都加锁（这个意思是部分锁，但不会锁表）
innodb下，串行化的读并不会锁表，而是根据where条件锁一个范围。但锁一个范围并不是简单锁记录，因为涉及到Next-key lock。


SELECT @@tx_isolation;
SET GLOBAL TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
SET GLOBAL TRANSACTION ISOLATION LEVEL READ COMMITTED;
SET GLOBAL TRANSACTION ISOLATION LEVEL REPEATABLE READ;
SET GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;


SHOW PROCESSLIST;
SHOW OPEN TABLES WHERE In_use > 0;
SELECT * FROM INFORMATION_SCHEMA.INNODB_LOCKS;
SELECT * FROM INFORMATION_SCHEMA.INNODB_LOCK_WAITS;
SELECT * FROM INFORMATION_SCHEMA.INNODB_TRX;
SHOW STATUS LIKE '%lock%';
SHOW VARIABLES LIKE '%timeout%';
```

