#!/bin/bash

if [ $# -lt 1 ];then
	echo "Usage: parse_taobao.sh [dir_name]"
	exit
fi
# base dir
base=/share/public/purchase
# work dir
wdir=$base/taobao/$1/

sum=$base/sum/products/$(basename $wdir).txt
tmp=$(mktemp)

cd $wdir

#转换为UTF8
for f in * 
do  
	enca -L zh_CN -x UTF-8 $f
done

# windows格式转换为Unix格式
for f in * 
do  
	dos2unix $f 
done

for f in *
do
	enca -L zh_CN -x UTF-8 $f
done

#合并指定行 -i 直接作用到源代码
cat * > $tmp
sed '/baobei-name/{N;s/\n/\t/}' $tmp -i
sed '/spec/{N;s/\n/\t/}' $tmp -i

#产品信息

egrep 'order-num|order-bd|baobei-name|class="spec"|class="price"|class="quantity"' $tmp | sed s/.*col.*//g | sed s/.*th.*//g | sed s/单价//g | sed s/数量//g | sed s/.*order-num../@order_num=/g | sed s/订单编号：//g | sed s/.*price../@product_price=/g | sed 's#</td>##g' | sed s/.*quantity../@product_quantity=/g | sed s/.*baobei-name../@product_name=/g | sed s/.*spec../@product_attr=/g | sed 's#.*id=.item#@item_id=#g' | sed 's#">##g' | sed '/order_num/{N;s/\n/\t/}' | sed '/item_id/{N;s/\n/\t/}'| sed '/product_name/{N;s/\n/\t/}' | sed '/product_attr/{N;s/\n/\t/}' | sed '/product_price/{N;s/\n/\t/}' | sed 's#<span>##g' | sed 's#</span>#  #g' | tee -a $sum

rm -f $tmp
