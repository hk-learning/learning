#!/bin/bash

if [ $# -lt 1 ];then
	echo "Usage: parse_taobao.sh [dir_name]"
	exit
fi
# base dir
base=/share/public/purchase
# work dir
wdir=$base/1688/$1/

sum=$base/sum/orders/$(basename $wdir).txt
tmp=$(mktemp)

cd $wdir

#转换为UTF8
for f in * 
do  
	enca -L zh_CN -x UTF-8 $f
done

# windows格式转换为Unix格式
for f in * 
do  
	dos2unix $f 
done

for f in *
do
	enca -L zh_CN -x UTF-8 $f
done

#合并指定行 -i 直接作用到源代码
cat * > $tmp
sed '/title-order/{N;s/\n/\t/}' $tmp -i
sed '/class="total"/{N;s/\n/\t/}' $tmp -i
sed '/class="fare"/{N;s/\n/\t/}' $tmp -i

#订单信息

egrep 'title-order|class="date"|bannerCorp|bannerMember|class="total"|class="fare"|class="gray"|class="black"|class="green"|class="gold-yellow"' $tmp | sed 's#.*</span></div>##g' | sed 's#.*left-token-count.*##g' | sed 's#</div>.*##g' | sed s/.*订单号./@order_num=/g | sed s/.*下单时间./@trans_date=/g | sed 's#</span>##g' | sed s/.*contact_company../@seller_name=/g | sed s/data-copytitle..//g | sed 's#..class=.bannerCorp.*##g' | sed s/.*html../@seller_id=/g | sed 's#..class=.bannerMember.*##g' | sed 's#.*含运费#@shipping_price=(#g' | sed s/.*title../@order_price=/g | sed 's#..class=.total.*##g' | sed 's#.*class=.gray..#@order_status=#g' | sed 's#.*class=.black..#@order_status=#g' | sed 's#.*class=.green..#@order_status=#g' | sed 's#.*class=.gold-yellow..#@order_status=#g' | sed '/order_num/{N;s/\n/\t/}' | sed '/trans_date/{N;s/\n/\t/}' | sed '/seller_name/{N;s/\n/\t/}' | sed '/seller_id/{N;s/\n/\t/}' | sed '/order_price/{N;s/\n/\t/}' | sed '/shipping_price/{N;s/\n/\t/}' | tee -a $sum

rm -f $tmp
