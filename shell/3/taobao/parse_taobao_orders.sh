#!/bin/bash
if [ $# -lt 1 ]
then
    echo "Usage: parse_taobao.sh [dir_name]"
    exit
fi

# base dir
base=/share/public/purchase/taobao/kehaoTest
# work dir
wdir=$base/$1/
sum=$base/target/$(basename $wdir).txt
tmp=$(mktemp)

cd $wdir

#转换为UTF8
#for f in * 
#do
#	enca -L zh_CN -x UTF-8 $f
#done

# Windows格式转换为Unix格式
#for f in * 
#do 
#	dos2unix $f 
#done

#转换为UTF8
#for f in *
#do
#	enca -L zh_CN -x UTF-8 $f
#done

#合并指定行 -i 直接作用到源代码
cat * > $tmp
tmp=/share/public/purchase/taobao/kehaoTest/source/1.txt  #后期删除掉
sed '/amount/{N;s/\n/\t/}' $tmp -i
sed '/post-type/{N;s/\n/\t/}' $tmp -i

#订单信息

grep -E "order-num|deal-time|nickname J_MakePoint|amount|post-type|J_MakePoint status" $tmp | 
sed s/.*col.*//g | 
sed s/.*th.*//g | 
sed 's#<label.*##g' | 
sed s/.*order-num../@order_num=/g | 
sed 's#</span>##g' | 
sed s/.*deal-time../@trans_date=/g | 
sed s/订单编号：//g | 
sed s/成交时间：//g | 
sed s/.*user_number_id./@seller_id=/g | 
sed 's#class=.nickname J_MakePoint.*##g' | 
sed 's#. title=# @seller_name=#g' | 
sed 's#.*amount..#@order_price=#g' |  
sed 's#<strong>##g'| 
sed 's#</strong>##g' | 
sed 's#.*post-type..#@shipping_price=#g' | 
sed 's#.含快递.##g'| 
sed 's#)##g' | 
sed 's#.*J_MakePoint status.*>#@orders_status=#g' | 
sed '/order_num/{N;s/\n/\t/}' | 
sed '/trans_date/{N;s/\n/\t/}' | 
sed '/seller_name/{N;s/\n/\t/}' | 
sed '/order_price/{N;s/\n/\t/}' | 
sed '/shipping_price/{N;s/\n/\t/}' | 
#tee -a $sum

#rm -f $tmp