#!/bin/bash

if [ $# -lt 1 ];then
	echo "Usage: parse_taobao.sh [dir_name]"
	exit
fi
# base dir
base=/share/public/purchase
# work dir
wdir=$base/1688/$1/

sum=$base/sum/products/$(basename $wdir).txt
tmp=$(mktemp)

cd $wdir

#转换为UTF8
for f in * 
do  
	enca -L zh_CN -x UTF-8 $f
done

# windows格式转换为Unix格式
for f in * 
do  
	dos2unix $f 
done

for f in *
do
	enca -L zh_CN -x UTF-8 $f
done

#合并指定行 -i 直接作用到源代码
cat * > $tmp
sed '/title-order/{N;s/\n/\t/}' $tmp -i
sed '/class="s3"/{N;s/\n/\t/}' $tmp -i
sed '/class="s4"/{N;s/\n/\t/}' $tmp -i

#产品信息

egrep 'title-order|class="productName"|class="semicolon"|class="s3"|class="s4"' $tmp | sed 's#.*</span></div>##g' | sed s/.*订单号./@order_num=/g | sed s/.*order_entry_id./@item_id=/g | sed 's#..class=.productName..# @product_name=#g' | sed 's#</a></div>##g' | sed 's#.font class..semicolon..##g' | sed 's#</font><strong>##g' | sed 's#</strong></span>##g' | sed 's#style..display..block...##g' | sed s/.*sku-item../@product_attr=/g | sed 's#</strong>;.*#;#g' | sed '/order_num/{N;s/\n/\t/}' | sed '/;/{N;s/\n/\t/}' | sed 's#;.*.@product_attr=#;#g' | sed 's#.*s3.*title..#@product_price=#g' | sed 's#.*s4.*title..#@product_quantity=#g' | sed 's#">##g' | sed '/product_name/{N;s/\n/\t/}' | sed '/product_attr/{N;s/\n/\t/}' | sed '/product_price/{N;s/\n/\t/}' | tee -a $sum


rm -f $tmp
