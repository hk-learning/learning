#!/bin/bash

if [ $# -lt 1 ];then
	echo "Usage: parse_taobao.sh [dir_name]"
	exit
fi
# base dir
base=/share/public/purchase
# work dir
wdir=$base/aliexpress/$1/

sum=$base/aliexpress/sum/$(basename $wdir).xlsx
tmp=$(mktemp)

cd $wdir

#转换为UTF8
for f in * 
do  
	enca -L zh_CN -x UTF-8 $f
done

# windows格式转换为Unix格式
for f in * 
do  
	dos2unix $f 
done

for f in *
do
	enca -L zh_CN -x UTF-8 $f
done

#合并指定行 -i 直接作用到源代码
cat * > $tmp
sed '/detail/{N;s/\n/\t/}' $tmp -i
sed '/detail/{N;s/\n/\t/}' $tmp -i

#产品信息

egrep 'picCore|atc-product-id|class="detail"|class="cost"' $tmp | sed s/.*pro-name.*//g | sed 's#..src=.*#}#g' | sed s/.*alt=./{/g | sed 's#..href=.*#@#g' | sed s/.*title..//g | sed 's#{.*\##{#g' | sed 's#{.*\[##g' | sed 's#|.*}#@#g' | sed 's#{##g' | sed 's#}#@#g' | sed s/.*value..//g | sed 's#..class=.atc-product-id.*#@#g' | sed 's#.*notranslate">US..##g' | sed 's#</b>.*#@#g' | sed '/@/{N;s/\n/\t/}'| sed '/@/{N;s/\n/\t/}' | sed 's#@##g' | sed '/^ *$/d' | tee -a $sum

rm -f $tmp
