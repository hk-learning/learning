#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os,sys

import urllib,urllib2

import lxml

import lxml.html as HTML

import lxml.etree as etree

reload(sys)
sys.setdefaultencoding("utf-8")


def g_trans(str_text,lang):
#def g_trans(str_text):

    lin = 'en'

    #lout = 'cn'

    values = {'hl':'lang', 'ie':'UTF-8', 'text':str_text, 'sl':lin, 'tl':lang}

    url = 'http://translate.google.cn/translate_t'

    data = urllib.urlencode(values)

    req = urllib2.Request(url, data)

    req.add_header('User-Agent', "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)")

    response = urllib2.urlopen(req, timeout = 10)

    htree = HTML.parse(response)

    response.close()

    #注意，此处返回的是一个list

    #emts = htree.xpath('/html/body/div[2]/div[2]/div[2]/div/div/div[2]/div')

    emts = htree.xpath('//*[@id="result_box"]')

    return emts[0].text_content()



print(g_trans(sys.argv[1],sys.argv[2]))
