#!/usr/bin/env python

import sys,os,hashlib,math
from datetime import datetime as dt

def get_channel(md5):
    v = int(md5[:1], 16)
    #4 nodes
    nodes_num = 4
    n = int(math.ceil(16/nodes_num))
    ranges = range(n-1, n*nodes_num+1, 4)
    return (k for k,x in enumerate(ranges) if v <= x).next()

def pack_redis_hash(key, dic):
    seg = '\r\n'
    hash_len = len(dic)*2+2;
    s = '*'+str(hash_len)+seg+'$5'+seg+'HMSET'+seg+'$'+str(len(key))+seg+key+seg
    for k,v in dic.items():
        s += '$'+str(len(k))+seg+k+seg+'$'+str(len(v))+seg+v+seg

    return s

def process_user_index(line):
    _id,person_id,area_code,phone,password,salt,state,lng,lat,hidden,user_cate_id,last_activity,created_at,updated_at,friend_num,contacts_num,contasts_user_num,is_adv_user,channel = line.split(tab)
    key = "d_user_index/%s_%s" % (area_code, phone)
    md5 = hashlib.md5(key).hexdigest()
    channel = "user%s" % str(get_channel(md5))
    dic = {'area_code': area_code, 'user_name': phone, 'user_id': _id, 'person_id': person_id}
    redis_data = pack_redis_hash(key, dic)
    return (channel, redis_data)

def process_phone_number(line):
    _id,person_id,from_user_id,area_code,number,hidden,created_at,updated_at,uploader_birthday = line.split(tab)
    key = "d_phone_number/%s_%s" % (area_code, number)
    md5 = hashlib.md5(key).hexdigest()
    channel = "main%s" % str(get_channel(md5))
    dic = {
        'id': _id,
        'person_id': person_id,
        'from_user_id': from_user_id,
        'area_code': area_code,
        'number': number,
        'hidden': hidden,
        'created_at': created_at,
        'updated_at': updated_at,
        'uploader_birthday': uploader_birthday,
    }
    redis_data = pack_redis_hash(key, dic)
    return (channel, redis_data)

if __name__ == '__main__':
    tables = ('user_index', 'phone_number')
    if len(sys.argv) < 4:
        print 'parameter error\nusage: ./convert_redis.py table csv_filepath dest_prefix'
        sys.exit()

    table = sys.argv[1]
    csv_filepath = os.path.realpath(sys.argv[2])
    dest_prefix = os.path.realpath(sys.argv[3])
    if not os.path.exists(csv_filepath):
        print 'csv file not exists'
        sys.exit()
    if not os.path.dirname(dest_prefix.rstrip(os.pathsep)):
        print 'dest prefix dir not exists'
        sys.exit()

    if table not in tables:
        print 'error table name'
        sys.exit()

    #same as get_channel()
    if table == 'user_index':
        nodes = ['user0', 'user1', 'user2', 'user3']
    else:
        #phone_number
        nodes = ['main0', 'main1', 'main2', 'main3']

    nodesf = {}
    for node in nodes:
        #./prefix_user2.data
        node_file = '%s_%s.data' % (dest_prefix, node)
        os.unlink(node_file) if os.path.exists(node_file) else False
        nodesf[node] = open(node_file, 'a')

    func = 'process_%s' % table
    tab = "#tab#"
    with open(csv_filepath) as f:
        for line in f:
            channel, redis_data = locals()[func](line.rstrip('\n'))
            nodesf[channel].write(redis_data)



