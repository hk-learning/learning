#!/usr/bin/env bash

test_ip="127.0.0.1"
dir="/data/go/bin/"

CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o user main.go

scp user root@${test_ip}:${dir}/user0

ssh root@${test_ip} > /dev/null 2>&1 << eeooff
cd /data/go/bin

if [ -f "user" ];then
    mv user user.bak
fi

if [ -f "user0" ];then
    mv user0 user
fi
exit
eeooff

ssh root@${test_ip} '/bin/kill `ps -ef | grep user | grep -v grep | awk '\'{print \$2}\''`';
ssh root@${test_ip} "nohup /data/go/bin/user > /tmp/user.log 2>&1 &"
