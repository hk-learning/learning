#!/usr/bin/env bash

echo 'Please input a URL:'
read url
if [ -z "${url}" ]; then
    echo 'URL can not be null!'
    exit 0
fi

code=$(curl -o /dev/null -s -m 100 --connect-timeout 10 -w %{http_code} ${url})

echo `date '+%Y-%m-%d %H:%M:%S'`
if [ "$code" = '200' ]; then
    echo 'OK'
else
    echo 'ERROR'
fi
