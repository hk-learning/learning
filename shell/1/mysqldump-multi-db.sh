#!/usr/bin/env bash

host=localhost
user=kehao
password=kehao
database='d_user_'
dir=/tmp

for i in 1 2 3
do
    mysqldump -h${host} -u${user} -p${password} ${database}${i} > "${dir}/${database}${i}.sql"
done
