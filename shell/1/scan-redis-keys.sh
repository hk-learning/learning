#!/bin/bash

ports=(7502 7503 7504)
keys_file=/tmp/scan_redis_keys.log
redis_cli=/opt/redis/bin/redis-cli

scan_redis() {
    server=192.168.1.5
    port=$1
    prefix=$2
    count=10000
    if [ -z "$1" ]; then
        echo "parameter #1 error"
    fi

    cursor=0
    while true; do
        echo -n $server
        echo -n $port
        echo $cursor
        if [[ $cursor -eq 0 ]]; then
            break;
        else
            $redis_cli -c -h $server -p $port scan $cursor MATCH "${prefix}*" COUNT $count|grep $prefix >> $keys_file
			cursor=`$redis_cli -c -h $server -p $port scan $cursor MATCH "${prefix}*" COUNT $count|head -n 1`
        fi
        sleep 1
    done
    return 0
}

for p in ${ports[@]}; do
    scan_redis $p "session_token"
done
