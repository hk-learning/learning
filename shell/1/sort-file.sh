#!/usr/bin/env bash

baseDir='/tmp/answerSlidePersonEachDay'
for file in $(ls "${baseDir}/ori")
do
    oldFile="${baseDir}/ori/${file}"
    newFile="${baseDir}/done/${file}"
    echo ${oldFile}
    echo ${newFile}
    sort ${oldFile} | uniq > ${newFile}
done
