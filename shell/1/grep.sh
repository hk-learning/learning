#!/bin/bash

echo 'Please input a keyword:'
read keyword
#if [ ${keyword} == '' ]; then
if [ -z "${keyword}" ]; then
    echo 'keyword can not be null!'
    exit 0
fi

echo 'Please input your specified path:'
read dir
#test ! -d ${dir} && echo "The ${dir} is not exist in your system." && exit 0
if [ ! -d "${dir}" ]; then
    echo "The ${dir} is not exits in your system."
    exit 0
fi

echo '---------------You find files are:---------------'

file_count=0
#递归查看所有目录，即最深路径，不显示空行
file_list=`ls -R ${dir} 2> /dev/null | grep -v '^$'`

for filename in ${file_list}
do
    #将文件名中所有匹配 ':',后接一个或多个任意字符（.代表任意字符,*代表0个或多个,$代表行尾结束符）全局(g)替换为无
    temp=`echo ${filename} | sed 's/:.*$//g'`
    #如果临时文件变量temp是一个目录，而非文件，就将该目录赋值给cur_dir变量
    #if [ "${filename}" != "${temp}" ]; then
    if [ -d "${temp}" ];then
        cur_dir=${temp}
        #echo ${cur_dir}
    else
        #用file命令查看文件真身是否为ASCII text类型
        file_type=`file ${cur_dir}/${filename} | grep 'text'`
        if [ "${file_type}" != '' ]; then
            temp=`grep ${keyword} ${cur_dir}/${filename} 2> /dev/null`
            if [ "${temp}" != '' ]; then
                echo ${cur_dir}/${filename}
                let file_count++
            fi
        fi
    fi
done

echo '-------------------------------------------------'
echo "Files Total: ${file_count}"
