#!/bin/bash

echo 'Please input a keyword:'
read keyword
#if [ ${keyword} == '' ]; then
if [ -z "${keyword}" ]; then
    echo 'keyword can not be null!'
    exit 0
fi

echo 'Please input your specified path:'
read dir
#test ! -d ${dir} && echo "The ${dir} is not exist in your system." && exit 0
if [ ! -d "${dir}" ]; then
    echo "The ${dir} is not exits in your system."
    exit 0
fi

echo '---------------You find files are:---------------'
echo `grep -r ${keyword} ${dir} 2>/dev/null`
echo '-------------------------------------------------'
