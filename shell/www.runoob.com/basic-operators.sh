#!/usr/bin/env bash

#http://www.runoob.com/linux/linux-shell-basic-operators.html
#Shell 基本运算符
    #表达式和运算符之间要有空格，例如 2+2 是不对的，必须写成 2 + 2，这与我们熟悉的大多数编程语言不一样
val=`expr 2 + 2`
echo "两数之和为 : $val"

#条件表达式要放在方括号之间，并且要有空格，例如: [$a==$b] 是错误的，必须写成 [ $a == $b ]
a=10
b=20

if [ $a -ne $b ]
then
    echo '-ne'
fi

#关系运算符 -eq -ne -gt -lt -ge -le
#布尔运算符 ! -o -a
#逻辑运算符 && ||
#字符串运算符 = != -z -n str
#文件测试运算符
