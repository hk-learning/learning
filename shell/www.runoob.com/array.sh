#!/usr/bin/env bash

#http://www.runoob.com/linux/linux-shell-array.html
#Shell 数组
#数组中可以存放多个值。Bash Shell 只支持一维数组（不支持多维数组），初始化时不需要定义数组大小（与 PHP 类似）。
#与大部分编程语言类似，数组元素的下标由0开始。
#Shell 数组用括号来表示，元素用"空格"符号分割开，语法格式如下：
#array_name=(value1 ... valuen)

my_array=(A B "C" D)
echo "第一个元素为: ${my_array[0]}"
echo "第二个元素为: ${my_array[1]}"
echo "第三个元素为: ${my_array[2]}"
echo "第四个元素为: ${my_array[3]}"

echo "数组的元素为: ${my_array[*]}"
echo "数组的元素为: ${my_array[@]}"

echo "数组元素个数为: ${#my_array[*]}"
echo "数组元素个数为: ${#my_array[@]}"
