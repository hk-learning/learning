#!/usr/bin/env bash

dec2hex(){
    printf "%x" $1
}

for i in `seq 1 24`
do
    a=$(dec2hex ${i})
    echo ${a}
done
