#!/usr/bin/env bash

########################
#if []
#then
    #comand
#fi

########################
#if []
#then
    #comand
#else
    #comand
#fi

########################
#if []
#then
    #comand
#elif
#then
    #comand
#else
    #comand
#fi

########################
#for id in 1 2 3 4 5
#do
#    echo ${id}
#done

for ((i=0; i<=3; i++))
do
    echo ${i}
done


int=1;
while [ $int -le 3 ]
do
    echo $int;
    let "int++"
done

echo '按下 <CTRL-D> 退出'
echo -n '输入你最喜欢的电影名: '
while read film
do
    echo "是的！$film 是一部好电影"
done
