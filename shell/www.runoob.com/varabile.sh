#!/usr/bin/env bash

#http://www.runoob.com/linux/linux-shell-variable.html
#Shell 变量
    #定义变量时，变量名不加美元符号（$，PHP语言中变量需要）
    #注意，变量名和等号之间不能有空格，这可能和你熟悉的所有编程语言都不一样。
your_name="runoob.com"
your_name='runoob.com'

#使用变量
    #使用一个定义过的变量，只要在变量名前面加美元符号即可
    #变量名外面的花括号是可选的，加不加都行，加花括号是为了帮助解释器识别变量的边界
    #推荐给所有变量加上花括号，这是个好的编程习惯
echo $your_name
echo ${your_name}

for skill in Ada Coffe Action Java; do
    echo "I am good at ${skill}Script"
done

#Shell 字符串
    #字符串是shell编程中最常用最有用的数据类型（除了数字和字符串，也没啥其它类型好用了）
    #字符串可以用单引号，也可以用双引号，也可以不用引号。单双引号的区别跟PHP类似
your_name='qinjx'
str='Hello, I know your are \"$your_name\"! \n'
echo ${str}
str="Hello, I know your are \"$your_name\"! \n"
echo ${str}

#拼接字符串
your_name="qinjx"
greeting="hello, "$your_name" !"
greeting_1="hello, ${your_name} !"
echo $greeting $greeting_1

#获取字符串长度
string="abcd"
echo ${#string} #输出 4

#提取子字符串
    #以下实例从字符串第 2 个字符开始截取 4 个字符
string="runoob is a great site"
echo ${string:1:4} # 输出 unoo

#查找子字符串
    #查找字符 "i 或 s" 的位置
string="runoob is a great company"
echo `expr index "$string" is`  # 输出 8

#Shell 数组
    #bash支持一维数组（不支持多维数组），并且没有限定数组的大小
    #类似与C语言，数组元素的下标由0开始编号。获取数组中的元素要利用下标，下标可以是整数或算术表达式，其值应大于或等于0

#定义数组
    # 在Shell中，用括号来表示数组，数组元素用"空格"符号分割开。定义数组的一般形式为：数组名=(值1 值2 ... 值n)

#读取数组元素值的一般格式是：${数组名[下标]}
#使用@符号可以获取数组中的所有元素，例如：echo ${array_name[@]}

#获取数组的长度: 获取数组长度的方法与获取字符串长度的方法相同

array=(1  2 3 'one' 'two'  4)
echo 'array length:' ${#array[@]}
echo 'array:' ${array[@]}

# 取得数组元素的个数 length=${#array_name[@]} 或者 length=${#array_name[*]}
# 取得数组单个元素的长度 lengthn=${#array_name[n]}

echo 'array each element:'
for data in ${array[@]};do
    echo ${data}
done

# Shell 注释
#以"#"开头的行就是注释，会被解释器忽略
#--------------------------------------------
# 这是一个注释
# author：菜鸟教程
# site：www.runoob.com
# slogan：学的不仅是技术，更是梦想！
#--------------------------------------------
##### 用户配置区 开始 #####
#
#
# 这里可以添加脚本描述信息
#
#
##### 用户配置区 结束  #####