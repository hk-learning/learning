#### 参考资料
- [RPC、IDL、IPC概念](https://www.jianshu.com/p/8236f39f2cdc)
- [RPC原理及实现\Stub](https://www.cnblogs.com/Luouy/p/7399908.html)

#### RPC 是什么
RPC 即 Remote Procedure Call (远程过程调用) 是一种计算机通讯协议，它为我们定义了计算机 C 中的程序如何调用另外一台计算机 S 的程序，让程序员不需要操心底层网络协议，使得开发包括网络分布式多程序在内的应用程序更加容易。

RPC 是典型的 Client/Server 模式，由客户端对服务器发出若干请求，服务器收到后根据客户端提供的参数进行操作，然后将执行结果返回给客户端。

RPC 位于 OSI 模型中的会话层：

![](../img/68d0a4f024a782c2.png)

#### IDL 是什么

RPC 只是一种协议，规定了通信的规则。

在实际工作中客户端与服务端会有各种各样的平台，就好像日常开发一样，为了统一处理不同的实现，需要定义一个共同的接口，于是有了 IDL。

IDL 即 Interface Description Language (接口定义语言)。

它通过一种中立的方式来描述接口，使得在不同平台上运行的对象和用不同语言编写的程序可以

相互通信交流。比如，一个组件用 C++ 写成，另一个组件用 Java 写，仍然可以通信。

![](../img/C8FA628F4158.png)

#### IPC 是什么

IPC 即 Inter-Process Communication (进程间通信)。

Android 基于 Linux，而 Linux 出于安全考虑，不同进程间不能之间操作对方的数据，这叫做“进程
隔离”。进程隔离”更详细的介绍（http://blog.csdn.net/u010132993/article/details/72582655）。


